#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later
"""
If a new version of the package `freedombox` is uploaded to backports,
build a new FreedomBox AMI.
"""

import re
from functools import reduce
from operator import add
from urllib.request import urlopen

import boto3


def get_latest_backports_package_version():
    """Return the latest version of backported freedombox package
    by scraping the Debian QA page.
    """
    response = urlopen("https://qa.debian.org/madison.php?package=freedombox")

    for line in response.read().decode().split('\n'):
        if 'backports' in line:
            return line.split('|')[1].strip()

    return ""


def get_latest_ami_names():
    """Returns the current list of owned AMI names.
    """
    ec2_client = boto3.client('ec2', region_name='eu-central-1')
    images = ec2_client.describe_images(Owners=['self'])
    return [image['Name'] for image in images['Images']]


def _compare_version_level(version1, version2, index):
    if version1.get(index) and version2.get(index):
        if version1[index] < version2[index]:
            return -1
        if version1[index] == version2[index]:
            if not version1.get(index + 1) and not version2.get(index + 1):
                return 0
            return _compare_version_level(version1, version2, index + 1)
        return 1
    if version1.get(index) and not version2.get(index):
        return 1
    return -1


def compare_versions(version1, version2):
    """Compare freedombox package versions."""
    size = max(len(version1), len(version2))
    version1 = dict(zip(range(size), map(int, version1.split('.'))))
    version2 = dict(zip(range(size), map(int, version2.split('.'))))
    return _compare_version_level(version1, version2, 0)


def is_new_image_required(ami_names, latest_version):
    """Return whether the we have a new backported package to build a new AMI.
    """
    debian_version = latest_version.split('~')[0]
    print("Debian version -", debian_version)
    # This assumes that there will only be one matching AMI
    for ami_name in ami_names:
        matches = list(
            map(lambda e: reduce(add, e),
                re.findall(r'([0-9]+[.][0-9]+)([.][0-9]+)?', ami_name)))
        if matches:
            latest_ami_version = matches[0]
            print("Latest AMI version -", latest_ami_version)
            return compare_versions(latest_ami_version, debian_version) == -1
    return False


def handler(event, _):
    """If a new version of the FreedomBox package is available on
    stable-backports, trigger AWS CodeBuild pipeline that builds a new AMI.
    """
    ami_names = get_latest_ami_names()
    latest_version = get_latest_backports_package_version()

    if latest_version and is_new_image_required(ami_names, latest_version):
        print("Building AMI for new version of FreedomBox:", latest_version)
        client = boto3.client('codebuild')
        client.start_build(projectName='cloud-image-builder')
    else:
        print("Current FreedomBox AMI is already at latest backports version")


def tests():
    assert (compare_versions("20.11", "20.12") == -1)
    assert (compare_versions("20.13", "20.12") == 1)
    assert (compare_versions("20.12", "20.12") == 0)
    assert (compare_versions("20.12.1", "20.12") == 1)
    assert (compare_versions("20.12", "20.12.1") == -1)


if __name__ == '__main__':
    tests()
    handler(None, None)
