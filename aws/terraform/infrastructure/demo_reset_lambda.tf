# This block is terraform boilerplate
resource "aws_iam_role" "demo_reset_lambda_role" {
  name = "DemoServerManager"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "archive_file" "demo_reset" {
  type        = "zip"
  source_file = "${path.module}/demo_reset_lambda.py"
  output_path = "${path.module}/files/demo_reset_lambda.zip"
}


# Lambda to periodically reset the demo server
resource "aws_lambda_function" "demo_reset_function" {
  function_name    = "demo_reset"
  role             = aws_iam_role.demo_reset_lambda_role.arn
  handler          = "demo_reset_lambda.handler"
  runtime          = "python3.8"
  filename         = "${path.module}/files/demo_reset_lambda.zip"
  source_code_hash = data.archive_file.demo_reset.output_base64sha256

  tags        = map("Name", "Lambda function to periodically reset demo.freedombox.org")
  timeout     = 600
  memory_size = 128
}

resource "aws_iam_policy" "demo_reset_policy" {
  description = "Allow managing demo server resets"
  name        = "DemoInstanceManagement"
  path        = "/"
  policy = jsonencode(
    {
      Statement = [
        {
          Action = [
            "ec2:DescribeImages",
            "ec2:RunInstances",
            "ec2:DisassociateAddress",
            "ec2:AssociateAddress",
            "ec2:DescribeAddresses",
            "ec2:CreateTags",
            "ec2:DescribeInstances",
            "ec2:DescribeInstanceStatus",
            "ec2:StopInstances",
            "ec2:TerminateInstances"
          ]
          Effect   = "Allow"
          Resource = "*"
          Sid      = "VisualEditor0"
        },
      ]
      Version = "2012-10-17"
    }
  )
}


resource "aws_iam_role_policy_attachment" "demo_reset_attachment" {
  role       = aws_iam_role.demo_reset_lambda_role.name
  policy_arn = aws_iam_policy.demo_reset_policy.arn
}


# Reusing the policy created in cleanup Lambda
resource "aws_iam_role_policy_attachment" "demo_reset_lambda_logging" {
  role       = aws_iam_role.demo_reset_lambda_role.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}


resource "aws_cloudwatch_log_group" "demo_reset_logs" {
  name              = "/aws/lambda/${aws_lambda_function.demo_reset_function.function_name}"
  retention_in_days = 14
}
