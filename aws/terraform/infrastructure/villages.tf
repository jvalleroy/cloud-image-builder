resource "aws_s3_bucket" "villages" {
  bucket = "freedombox-for-villages"
  acl    = "public-read"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    enabled = true
    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }
    transition {
      days          = 90
      storage_class = "GLACIER"
    }
  }

  tags = {
    Description = "Bucket to store artifacts for the FreedomBox for Villages project."
  }
}
