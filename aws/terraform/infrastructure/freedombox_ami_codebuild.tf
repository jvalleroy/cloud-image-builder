#######
# IAM #
#######

resource "aws_iam_policy" "packer" {
  description = "Policy to let an EC2 instance become a Packer builder"
  name        = "PackerEC2Builder"
  path        = "/"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "ec2:AttachVolume",
            "ec2:AuthorizeSecurityGroupIngress",
            "ec2:CopyImage",
            "ec2:CreateImage",
            "ec2:CreateKeypair",
            "ec2:CreateSecurityGroup",
            "ec2:CreateSnapshot",
            "ec2:CreateTags",
            "ec2:CreateVolume",
            "ec2:DeleteKeyPair",
            "ec2:DeleteSecurityGroup",
            "ec2:DeleteSnapshot",
            "ec2:DeleteVolume",
            "ec2:DeregisterImage",
            "ec2:DescribeImageAttribute",
            "ec2:DescribeImages",
            "ec2:DescribeInstances",
            "ec2:DescribeInstanceStatus",
            "ec2:DescribeRegions",
            "ec2:DescribeSecurityGroups",
            "ec2:DescribeSnapshots",
            "ec2:DescribeSubnets",
            "ec2:DescribeTags",
            "ec2:DescribeVolumes",
            "ec2:DetachVolume",
            "ec2:GetPasswordData",
            "ec2:ModifyImageAttribute",
            "ec2:ModifyInstanceAttribute",
            "ec2:ModifySnapshotAttribute",
            "ec2:RegisterImage",
            "ec2:RunInstances",
            "ec2:StopInstances",
            "ec2:TerminateInstances"
          ],
          "Resource" : "*"
        }
      ]
  })
}


resource "aws_iam_policy" "codebuild_base_policy_cloud_image_builder" {
  description = "Policy used in trust relationship with CodeBuild"
  name        = "CodeBuildBasePolicy-cloud-image-builder-eu-central-1"
  path        = "/service-role/"
  policy = jsonencode(
    {
      Statement = [
        {
          Action = [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
          ]
          Effect = "Allow"
          Resource = [
            "arn:aws:logs:eu-central-1:809319247576:log-group:/aws/codebuild/cloud-image-builder",
            "arn:aws:logs:eu-central-1:809319247576:log-group:/aws/codebuild/cloud-image-builder:*",
          ]
        },
        {
          Action = [
            "s3:PutObject",
            "s3:GetObject",
            "s3:GetObjectVersion",
            "s3:GetBucketAcl",
            "s3:GetBucketLocation",
          ]
          Effect = "Allow"
          Resource = [
            "arn:aws:s3:::codepipeline-eu-central-1-*",
          ]
        },
        {
          Action = [
            "codebuild:CreateReportGroup",
            "codebuild:CreateReport",
            "codebuild:UpdateReport",
            "codebuild:BatchPutTestCases",
          ]
          Effect = "Allow"
          Resource = [
            "arn:aws:codebuild:eu-central-1:809319247576:report-group/cloud-image-builder-*",
          ]
        },
      ]
      Version = "2012-10-17"
    }
  )
}

resource "aws_iam_policy" "codebuild_cloudwatch_logs_policy_cloud_image_builder" {
  description = "Policy used in trust relationship with CodeBuild"
  name        = "CodeBuildCloudWatchLogsPolicy-cloud-image-builder-eu-central-1"
  path        = "/service-role/"
  policy = jsonencode(
    {
      Statement = [
        {
          Action = [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents",
          ]
          Effect = "Allow"
          Resource = [
            "arn:aws:logs:eu-central-1:809319247576:log-group:cloud-image-builder",
            "arn:aws:logs:eu-central-1:809319247576:log-group:cloud-image-builder:*",
          ]
        },
      ]
      Version = "2012-10-17"
    }
  )
}

# resource "aws_iam_service_linked_role" "freedombox_ami_codebuild_role" {
#   aws_service_name = "codebuild.amazonaws.com"
# }

# resource "aws_iam_role_policy_attachment" "packer_attachment" {
#   role       = aws_iam_service_linked_role.freedombox_ami_codebuild_role.name
#   policy_arn = aws_iam_policy.packer.arn
# }

# resource "aws_iam_role_policy_attachment" "codebuild_base_policy_cloud_image_builder_attachment" {
#   role       = aws_iam_service_linked_role.freedombox_ami_codebuild_role.name
#   policy_arn = aws_iam_policy.codebuild_base_policy_cloud_image_builder.arn
# }

# resource "aws_iam_role_policy_attachment" "codebuild_cloudwatch_logs_cloud_image_builder_attachment" {
#   role       = aws_iam_service_linked_role.freedombox_ami_codebuild_role.name
#   policy_arn = aws_iam_policy.codebuild_cloudwatch_logs_policy_cloud_image_builder.arn
# }


#############
# CodeBuild #
#############

resource "aws_codebuild_project" "freedombox_ami_build" {
  badge_enabled  = false
  build_timeout  = 60
  description    = "Build FreedomBox AMI images using Packer"
  encryption_key = "arn:aws:kms:eu-central-1:809319247576:alias/aws/s3"
  name           = "cloud-image-builder"
  queued_timeout = 480
  service_role   = "arn:aws:iam::809319247576:role/service-role/codebuild-cloud-image-builder-service-role"
  tags           = {}

  artifacts {
    encryption_disabled    = false
    override_artifact_name = false
    type                   = "NO_ARTIFACTS"
  }

  cache {
    modes = []
    type  = "NO_CACHE"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/standard:4.0"
    image_pull_credentials_type = "CODEBUILD"
    privileged_mode             = false
    type                        = "LINUX_CONTAINER"
  }

  logs_config {
    cloudwatch_logs {
      group_name  = "cloud-image-builder"
      status      = "ENABLED"
      stream_name = "freedombox-packer"
    }

    s3_logs {
      encryption_disabled = false
      status              = "DISABLED"
    }
  }

  source {
    buildspec           = <<EOF
         version: 0.2
         phases:
           build:
             commands:
                - echo "Cloning cloud-image-builder"
                - git clone https://salsa.debian.org/freedombox-team/cloud-image-builder.git
                - echo "Installing HashiCorp Packer..."
                - curl -qL -o packer.zip https://releases.hashicorp.com/packer/1.6.0/packer_1.6.0_linux_amd64.zip && unzip packer.zip
                - echo "Installing ansible..."
                - pip3 install ansible
                - echo "Setting latest version"
                - export LATEST_VERSION=$(curl -s https://qa.debian.org/madison.php\?package\=freedombox | grep buster-backports | cut -f 2 -d "|" | tr -d '[:space:]')
                - echo "Validating Packer template for FreedomBox"
                - cd cloud-image-builder/aws
                - ../../packer validate packers/freedombox/freedombox-packer.json
                - echo "Building FreedomBox AMI"
                - ../../packer build packers/freedombox/freedombox-packer.json
     EOF
    git_clone_depth     = 1
    insecure_ssl        = false
    report_build_status = false
    type                = "NO_SOURCE"
  }
}
