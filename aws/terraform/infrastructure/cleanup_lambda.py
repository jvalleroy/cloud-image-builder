"""
Cleanup any leftover AMIs and snapshots after Packer build.
"""

import boto3

from botocore.exceptions import ClientError


def delete_older_images(ec2_client):
    """Delete all but the latest FreedomBox AMI in the AWS region."""
    images = ec2_client.describe_images(Owners=['self'])['Images']
    sorted_images = sorted(images, key=lambda image: image['CreationDate'])
    filtered_images = filter(
        lambda image: image['Name'].startswith('freedombox-buster-amd64'),
        sorted_images)
    for image in list(filtered_images)[:-1]:
        ec2_client.deregister_image(ImageId=image['ImageId'])


def delete_older_snapshots(ec2_client):
    """Delete all possible snapshots in the AWS region."""
    snapshots = ec2_client.describe_snapshots(OwnerIds=['self'])['Snapshots']
    for snapshot in snapshots:
        try:
            ec2_client.delete_snapshot(SnapshotId=snapshot['SnapshotId'])
        except ClientError as err:
            if 'is currently in use by ami' in err.response['Error'][
                    'Message']:
                continue


def handler(event, _):
    """Clean up all """
    client = boto3.client('ec2')
    regions = [
        region['RegionName'] for region in client.describe_regions()['Regions']
    ]

    for region in regions:
        ec2_client = boto3.client('ec2', region_name=region)
        print('Cleaning images in region', region)
        delete_older_images(ec2_client)
        print('Cleaning snapshots in region', region)
        delete_older_snapshots(ec2_client)


# This is for testing on the CLI locally
if __name__ == '__main__':
    handler(None, None)
