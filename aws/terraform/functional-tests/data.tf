data "aws_ami" "debian_unstable" {
  most_recent = true

  filter {
    name   = "name"
    values = "debian-unstable-for-freedombox"
  }

  owners = ["809319247576"]
}
