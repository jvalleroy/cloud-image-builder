# The system under test
resource "aws_instance" "app_server" {
  ami = data.aws_ami.debian_unstable.image_id
  instance_type = "t3a.small"

  tags {
    Name = "App Server"
  }
}


# TODO: Functional tests must depend on the app_server being available first

# resource "aws_instance" "functional_tests" {
#   ami = data.aws_ami.debian_unstable.image_id
#   instance_type = "t3a.small"
# }
