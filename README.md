# FreedomBox Cloud Image Builder

Build images of FreedomBox for various cloud providers.

## Supported cloud service providers
- AWS EC2

## Tools used

- Hashicorp Terraform
- Hashicorp Packer + Ansible
- Python3 with boto3 library

## Infrastructure as Code

Most of the deployed infrastructure on AWS is captured as code except where there Terraform still didn't have support for some of the newer services in AWS.

You just have to run `terraform apply` in the directory aws/terraform/infrastructure to get the entire infrastructure set up in a new AWS account (well, at least most of it).

## Build pipeline

> Build Trigger Lambda -> AWS CodeBuild pipeline -> Cleanup Lambda

### Build Trigger

This Lambda function does a daily check for availability of new versions of the `freedombox` package from Debian buster-backports repository. It then compares the version of the deb package with the version of the latest available FreedomBox AMI. If the deb package is newer, then a new build is triggered on CodeBuild.


### CodeBuild Pipeline

The CodeBuild pipeline requires a manual trigger since it cannot be configured to listen to our source code repository hosted on GitLab. This manual trigger is automatically triggered by the Lamdba function mentioned above. The pipeline runs `packer build` to build the FreedomBox AMI. Packer internally uses Ansible for installing the latest freedombox*.deb packages.

### Cleanup Lambda

This Lambda function goes over all the AWS regions to find and delete older AMIs and their associated volumes.


### Lambda triggers

AWS Event Bridge is currently being used to trigger our Lambda functions. We have the following triggers currently. This is worth documenting here since they aren't captured as Terraform code yet.

1. Daily trigger for the FreedomBox AMI build trigger Lambda
2. CodeBuild Pipeline's SUCCEEDED state as a trigger for the cleanup Lambda
3. A weekly trigger for cleanup Lambda that runs on Sunday at 00:00 as an additional measure


## Demo Server

The FreedomBox demo server deloyed at https://demo.freedombox.org is also managed by infrastructure code in this repository.

The process for creation of the demo server is manual and documented here:  
https://salsa.debian.org/freedombox-team/freedombox/snippets/277

### Demo Reset Lambda

This Lambda function resets the demo server at every 30th minute on the clock. It is triggered by a Scheduled Event from AWS Event Bridge.
